{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# WISPcloud API tutorial\n",
    "\n",
    "For accessing data collected by WISPstation instruments, [Water Insight](http://www.waterinsight.nl/ \"WI's Homepage\") offers an application programming interface (API). This API allows users to dynamically retrieve data from the WISPcloud database using a URL. This document is a short introduction to the WISPcloud API. Please note that this is still very much work in progress. Any feedback is appreciated!\n",
    "\n",
    "## Accessing the API\n",
    "There are several ways to access the API:\n",
    "* Using a web browser. Simply type or copy the request into the address bar and the browser will display the text file.\n",
    "* Using a command-line tool such as [cURL](https://curl.haxx.se// \"cURL Homepage\") \n",
    "* Directly in a script \n",
    "\n",
    "We will show examples for these options below. \n",
    "\n",
    "The API is password protected. As a user of a WISPstation, you should have received login credentials. There is also an open demo account:\n",
    "> username: demo\n",
    "\n",
    "> password: demo\n",
    "\n",
    "### Access via a web browser\n",
    "Simply paste the url in your browser's address bar. For a quick view, such as the ShowRecent request, using your browser is an easy and convenient way to access the API. Please note that this is not advised when retrieving larger amounts of data as browsers may at a given point stop downloading additional data without producing an error message. \n",
    "\n",
    "### Accessing via a command-line tool\n",
    "For larger amounts of data or as part of a recurrent processing, it can be useful to download data from the API to file. This can be done using the [cURL](https://curl.haxx.se// \"cURL Homepage\")  tool.\n",
    "\n",
    "```\n",
    "curl -u username:password -o filename.tsv \"https://wispcloud.waterinsight.nl/api/query?SERVICE=Data&VERSION=1.0&REQUEST=GetData&TIME=2018-05-11T09:00,2018-05-11T19:00&INSTRUMENT=WISPstation001\"\n",
    "``` \n",
    "\n",
    "### Accessing the API directly in python\n",
    "If working with the data in a script/program, the easiest way is to directly access the API from your script and read the returned data. For example, in python you could \n",
    "\n",
    "```python\n",
    "import requests\n",
    "import io\n",
    "\n",
    "username = 'demo'\n",
    "password = 'demo'\n",
    "url = 'https://wispcloud.waterinsight.nl/api/query?SERVICE=Data&VERSION=1.0&REQUEST=GetData&TIME=2018-05-11T09:00,2018-05-11T19:00&INSTRUMENT=WISPstation001'\n",
    "datastring = io.StringIO(requests.get(url, auth=(username, password)).content.decode('utf-8')).read()\n",
    "```\n",
    "\n",
    "## Building the request URL\n",
    "The request URL is built on a base URL: \n",
    "> https:wispcloud.waterinsight.nl/api/query?\n",
    "\n",
    "followed by several key-value pairs which are built up as follows:\n",
    "* values are assigned to keys by a \"=\"\n",
    "* several values for the same key are separated by \",\"\n",
    "* key-value pairs are separated by \"&\"\n",
    "\n",
    "All our requests use the service \"Data\". The current version is 1.0. Therefore, all request URLs will start with \n",
    "> https:wispcloud.waterinsight.nl/api/query?SERVICE=Data&VERSION=1.0\n",
    "\n",
    "followed by the specific request (see below). The keys are case-insensitive.\n",
    "\n",
    "## The requests\n",
    "\n",
    "### GetDocumentation\n",
    "\n",
    "The simplest request is GetDocumentation. It returns a static text-based documentation of the API:\n",
    "> https://wispcloud.waterinsight.nl/api/query?SERVICE=Data&VERSION=1.0&REQUEST=GetDocumentation\n",
    "\n",
    "### ShowRecent\n",
    "The ShowRecent request returns a fixed-format file of the most recent WISPstation measurements in WISPcloud that the user has access to. \n",
    "> https://wispcloud.waterinsight.nl/api/query?SERVICE=Data&VERSION=1.0&REQUEST=ShowRecent\n",
    "\n",
    "The reply is a comma-separated file giving the measurement id, device name and timestamp of the 30 most recent measurements that the user has access to:\n",
    "```\n",
    "measurement,device,timestamp\n",
    "85629,WISPstation004,2018-07-18 10:30:05.398616\n",
    "85633,WISPstation001,2018-07-18 10:30:05.369909\n",
    "85628,WISPstation002,2018-07-18 10:30:05.215879\n",
    "85630,WISPstation005,2018-07-18 10:30:04.843113\n",
    "85632,WISPstation005,2018-07-18 10:15:05.298292\n",
    "85626,WISPstation004,2018-07-18 10:15:05.280844\n",
    "...\n",
    "```\n",
    "\n",
    "### GetData\n",
    "The GetData request is the main work horse of the WISPcloud API. It allows to flexibly retrieve data from the WISPcloud database. The reply is always a tab-separated file containing the requested data. If used without any further specifier\n",
    "> https://wispcloud.waterinsight.nl/api/query?SERVICE=Data&VERSION=1.0&REQUEST=GetData\n",
    "\n",
    "it will return a selected water quality parameters and measurement meta-data for the latest 100 measurements that the user has access to:\n",
    "```\n",
    "\n",
    "# HEADERLINES 18\n",
    "# Request processed at 2018-07-18 10:41:16 UTC\n",
    "# \n",
    "# Requested by manager from 86.91.109.85\n",
    "# \n",
    "# ---original request---\n",
    "# https://wispcloud.waterinsight.nl/api/query?SERVICE=Data&VERSION=1.0&REQUEST=GetData\n",
    "# \n",
    "# ----------------------\n",
    "# Resource: https://wispcloud.waterinsight.nl/api/query\n",
    "# Parameters:\n",
    "#   service ['data']\n",
    "#   version ['1.0']\n",
    "#   request ['getdata']\n",
    "# ----------------------\n",
    "# \n",
    "# WARNING: No filter keywords were given in the query to keep the number of results in check. Output is capped to the most recent 100 measurements this account has access to until at least one filter is used in query.\n",
    "# \n",
    "instrument.name\tmeasurement.date\tmeasurement.id\twaterquality.tsm\twaterquality.chla\twaterquality.kd\n",
    "\tYYYY-MM-DD hh:mm:ss.sssss\t\tg m-3\tmg m-3\tm-1\n",
    "WISPstation004\t2018-07-18 10:30:05.398616\t85629\t4.4\t6.6\t3.8\n",
    "WISPstation001\t2018-07-18 10:30:05.369909\t85633\t12.0\t11.5\t0.7\n",
    "WISPstation002\t2018-07-18 10:30:05.215879\t85628\t41.8\tNone\t9.2\n",
    "WISPstation005\t2018-07-18 10:30:04.843113\t85630\t17.1\t27.4\t2.2\n",
    "WISPstation005\t2018-07-18 10:15:05.298292\t85632\t17.0\t27.2\t2.2\n",
    "WISPstation004\t2018-07-18 10:15:05.280844\t85626\t4.1\t8.7\t2.4\n",
    "WISPstation001\t2018-07-18 10:15:05.215745\t85627\t11.9\t11.2\t0.7\n",
    "WISPstation002\t2018-07-18 10:15:04.841156\t85625\t10.1\t11.2\t1.6\n",
    "...\n",
    "```\n",
    "\n",
    "As can be seen, the return format is a tab-separated text file. A variable-length header is indicated with lines starting with \"#\". The first header line indicates the length of the header. The next line after the file header contains the column names for the result columns, the line after that units and other meta-data necessary for interpretation of the results. After that follows one line per measurement that the request retrieved. Spectra are given in one column in brackets. Missing data are indicated by None.\n",
    "\n",
    "#### Selecting by date, location and instrument\n",
    "To specify which data to retrieve, the query can be narrowed by date/time, location and/or instrument name.\n",
    "\n",
    "##### Time\n",
    "To restrict the query to a certain date/time range, use the TIME keyword, specifying a time interval by two ISO timestamps separated by a comma. All times are in UTC.\n",
    "\n",
    "> https://wispcloud.waterinsight.nl/api/query?SERVICE=Data&VERSION=1.0&REQUEST=GetData&TIME=2018-05-11T09:00,2018-05-11T19:00\n",
    "\n",
    "If the TIME keyword is provided more than once, results matching EITHER range are returned, e.g. adding these parameters will select measurements from between 9:00 and 19:00 on the 11th and 16th of May 2018.\n",
    "\n",
    "> https://wispcloud.waterinsight.nl/api/query?SERVICE=Data&VERSION=1.0&REQUEST=GetData&TIME=2018-05-11T09:00,2018-05-11T19:00&TIME=2018-05-16T09:00,2018-05-16T19:00\n",
    "\n",
    "##### Instrument\n",
    "\n",
    "To retrieve data only from one or more specific instrument(s), use the INSTRUMENT keyword. If requesting more than on instrument, separate the instruments by \",\".\n",
    "\n",
    "> https://wispcloud.waterinsight.nl/api/query?SERVICE=Data&VERSION=1.0&REQUEST=GetData&TIME=2018-05-11T09:00,2018-05-11T19:00&INSTRUMENT=WISPstation001\n",
    "\n",
    "> https://wispcloud.waterinsight.nl/api/query?SERVICE=Data&VERSION=1.0&REQUEST=GetData&TIME=2018-07-11T09:00,2018-07-11T19:00&INSTRUMENT=WISPstation001,WISPstation005\n",
    "\n",
    "##### Location\n",
    "To select data within a certain geographic area, use the BBOX keyword. The format for the argument is BBOX=<west edge longitude>, <south edge latitude>, <east edge longitude>, <north edge latitude>\n",
    "\n",
    "If the BBOX parameter is specified more than once, records from either area will be included. Uniqueness of records in the output set is preserved, so when areas overlap, records in the overlap area are only added to the result set once.\n",
    "\n",
    "> https://wispcloud.waterinsight.nl/api/query?SERVICE=Data&VERSION=1.0&REQUEST=GetData&TIME=2018-07-11T09:00,2018-07-11T19:00&BBOX=12.0,43.0,12.2,43.2\n",
    "\n",
    "#### Selecting specific output columns\n",
    "To specify which information per measurement the results should include, use the INCLUDE keyword. When using the INCLUDE keyword, list all the columns you want in the output. Standard columns you will want to include are measurement.id and measurement.date (which includes a UTC timestamp). If you request a column that does not exist, the result will return a column of that name with all nodata (None) values.\n",
    "\n",
    "##### Retrieving water quality parameters\n",
    "As the previous examples already showed, it is possible to retrieve water quality parameters derived from the spectral measurements. The parameters that are available are with their respective column names are:\n",
    "- Chlorophyll-a concentration (based on Gons 1999): waterquality.chla\n",
    "- Total supsended matter (or suspended particulate matter) concentration (based on Rijkeboer 1999): waterquality.tsm\n",
    "- Vertical diffuse attenuation coefficient (based on Gons 1998): waterquality.kd\n",
    "- C-phycocyanin concentration (based on Simis 2005): waterquality.cpc\n",
    "\n",
    "A return file will look like this:\n",
    "```\n",
    "# HEADERLINES 18\n",
    "# Request processed at 2018-07-19 09:33:58 UTC\n",
    "# \n",
    "# Requested by manager from 92.111.24.140\n",
    "# \n",
    "# ---original request---\n",
    "# https://wispcloud.waterinsight.nl/api/query?SERVICE=Data&VERSION=1.0&REQUEST=GetData&TIME=2018-07-11T09:00,2018-07-11T19:00&INCLUDE=measurement.id,measurement.date,instrument.name,waterquality.chla,waterquality.tsm,waterquality.kd,waterquality.cpc\n",
    "# \n",
    "# ----------------------\n",
    "# Resource: https://wispcloud.waterinsight.nl/api/query\n",
    "# Parameters:\n",
    "#   include ['measurement.id,measurement.date,instrument.name,waterquality.chla,waterquality.tsm,waterquality.kd,waterquality.cpc']\n",
    "#   request ['getdata']\n",
    "#   time ['2018-07-11t09:00,2018-07-11t19:00']\n",
    "#   version ['1.0']\n",
    "#   service ['data']\n",
    "# ----------------------\n",
    "# \n",
    "measurement.id\tmeasurement.date\tinstrument.name\twaterquality.chla\twaterquality.tsm\twaterquality.kd\twaterquality.cpc\n",
    "\tYYYY-MM-DD hh:mm:ss.sssss\t\tmg m-3\tg m-3\tm-1\tmg m-3\n",
    "84010\t2018-07-11 09:00:05.333859\tWISPstation003\tNone\t77.5\tNone\t54.3\n",
    "84011\t2018-07-11 09:00:05.176993\tWISPstation002\t8.1\t11.7\t1.6\t7.4\n",
    "84013\t2018-07-11 09:00:05.346611\tWISPstation001\t7.7\t14.1\t1.0\tNone\n",
    "84014\t2018-07-11 09:15:05.083525\tWISPstation003\tNone\t102.4\tNone\t63.9\n",
    "84015\t2018-07-11 09:15:05.677694\tWISPstation002\t7.1\t8.8\t0.9\t2.5\n",
    "84017\t2018-07-11 09:15:05.719169\tWISPstation005\t20.5\t14.2\t2.3\t28.0\n",
    "84019\t2018-07-11 09:15:05.244534\tWISPstation001\tNone\t34.2\t6.4\tNone\n",
    "...\n",
    "```\n",
    "\n",
    "Currently only one algorithm is implemented per parameter. It is planned to provide different algorithms to accomodate for different water types. The syntax will then be waterquality.chla.gons for the algorithm of Gons.\n",
    "> https://wispcloud.waterinsight.nl/api/query?SERVICE=Data&VERSION=1.0&REQUEST=GetData&TIME=2018-07-11T09:00,2018-07-11T19:00&INCLUDE=measurement.id,measurement.date,instrument.name,waterquality.chla,waterquality.tsm,waterquality.kd,waterquality.cpc\n",
    "\n",
    "##### Retrieving spectral data\n",
    "\n",
    "> Please note that queries that return spectral data can result in rather large files. When testing please limit the number of rows you retrieve.\n",
    "\n",
    "The WISPstation measures downwelling irradiance, downwelling radiance and upwelling radiance. From these measurements, remote sensing reflectance is derived. The WISPstation measures in two directions, one of these directions is selected based on the time of day and instrument viewing azimuth angle (which ideally should be north, but can be different according to local constraints). So when requesting the radiance, irradiance and reflectance spectra, the query will yield the measurements of the selected channel. The following \n",
    "- Downwelling irradiance: ed.irradiance\n",
    "- Downwelling radiance: ld.radiance\n",
    "- Upwelling radiance: lu.radiance\n",
    "- Remote sensing reflectance: level2.reflectance\n",
    "The remote sensing reflectance is calculated from the irradiance and radiance measurements using a fixed rho of 0.028\n",
    "\n",
    "> https://wispcloud.waterinsight.nl/api/query?SERVICE=Data&VERSION=1.0&REQUEST=GetData&TIME=2018-07-11T09:00,2018-07-11T19:00&INCLUDE=measurement.id,measurement.date,instrument.name,ed.irradiance,ld.radiance,lu.radiance,level2.reflectance\n",
    "\n",
    "A resulting file will look like this:\n",
    "```\n",
    "# HEADERLINES 18\n",
    "# Request processed at 2018-07-19 09:40:16 UTC\n",
    "# \n",
    "# Requested by manager from 92.111.24.140\n",
    "# \n",
    "# ---original request---\n",
    "# https://wispcloud.waterinsight.nl/api/query?SERVICE=Data&VERSION=1.0&REQUEST=GetData&TIME=2018-07-11T09:00,2018-07-11T19:00&INCLUDE=measurement.id,measurement.date,instrument.name,ed.irradiance,ld.radiance,lu.radiance,level2.reflectance\n",
    "# \n",
    "# ----------------------\n",
    "# Resource: https://wispcloud.waterinsight.nl/api/query\n",
    "# Parameters:\n",
    "#   include ['measurement.id,measurement.date,instrument.name,ed.irradiance,ld.radiance,lu.radiance,level2.reflectance']\n",
    "#   request ['getdata']\n",
    "#   time ['2018-07-11t09:00,2018-07-11t19:00']\n",
    "#   version ['1.0']\n",
    "#   service ['data']\n",
    "# ----------------------\n",
    "# \n",
    "measurement.id\tmeasurement.date\tinstrument.name\ted.irradiance\tld.radiance\tlu.radiance\tlevel2.reflectance\n",
    "\tYYYY-MM-DD hh:mm:ss.sssss\t\tW/(m2*nm) for wavelength [350..900] in 1nm steps\tW/(m2*nm*sr) for wavelength [350..900] in 1nm steps\tW/(m2*nm*sr) for wavelength [350..900] in 1nm steps\t1/sr for wavelength [350..900] in 1nm steps\n",
    "84010\t2018-07-11 09:00:05.333859\tWISPstation003\t[0.06620044,0.06827728,...,0.05627904]\t[0.00525995,0.00541506,...,0.00527671]\t[0.00463759,0.00476673,...,0.00470537]\t[0.06782901,0.06759363,...,0.08098264]\n",
    "...\n",
    "```\n",
    "\n",
    "It is also possible to retrieve the measurements of specific channels, irrespective of whether or not they are the \"selected\" channel for the specific measurement. The channels are as follows:\n",
    "\n",
    "- eda.irradiance: Ed at the back 'aft' top of the instrument\n",
    "- edf.irradiance: Ed at the front 'fore' top of the instrument\n",
    "- ldp.radiance: Ld channel at the left 'port' front of the instrument\n",
    "- lds.radiance: Ld channel at the right 'starboard' front of the instrument\n",
    "- lup.radiance: Lu channel at the left 'port' front of the instrument\n",
    "- lus.radiance: Lu channel at the right 'starboard' front of the instrument\n",
    "\n",
    "Please note that the radiance channels are \"cross-eyed\", i.e. they look across the center line of the instrument. So the right \"starboar\" Lu and Ld channels look at 337.5 degree and the left \"port\" channels at 22.5 from the instrument orientation, usually north. \n",
    "\n",
    "To get the full orientation, you can combine the information about the instrument's orientation as a whole with the orientation of each channel with respect to the instrument orientation. The instrument's orientation is available as site.azimuth. The channel orientations of the radiance channels as ldp.channel.orientation, lds.channel.orientation, lup.channel.orientation, lus.channel.orientation. The Ed channels are both facing straight up and therefore have an orientation of 0.\n",
    "\n",
    "To find out which channel was selected for a specific measurement, use the keywords ed.selected, ld.selected and lu.selected.\n",
    "\n",
    "It is important to recognize that INCLUDE parameters are only about what is displayed, not which way the calculation goes. Including explicitly the data from certain channels in your output does NOT make those the channels used in the reflectance and subsequent water quality calculations! It only means you have the option to also see what was in the channel not used in the calculation.\n",
    "\n",
    "##### Quality flagging\n",
    "Some basic quality screening is done on the spectra. Based on a number of flags, each spectrum gets a quality classification as 'okay', 'suspect' or 'invalid'. These quality indicators can be retrieved using the columns ed.quality, ld.quality, lu.quality and level2.quality.\n",
    "\n",
    "Please note that due to very restrictive flagging, currently almost all measurements are shown as 'suspect'. At this point, please ignore this we will be checking and updating the quality flagging.\n",
    "\n",
    "!!! Important caveat: The level2.quality column has a very specific functionality: Per default, measurements where level2.quality is 'invalid' will not be shown in the output. Adding this column not only adds a column to the output, but as explicitly requesting the quality implies that the user is aware of the possible different quality levels of data it also makes visible the records in your result set where the quality is below the standard of quality that is shown by default.\n",
    "\n",
    "\n",
    "##### Additional information\n",
    "Some additional information about the measurement, the instrument, and the measurement site can be requested using the following columns:\n",
    "- measurement.date: Date and time of the measurement\n",
    "- measurement.id: Unique identifier for this measurement\n",
    "- measurement.owner: Names the account that owns this measurement, if there is a defined data owner\n",
    "- instrument.name: The common name of the instrument.\n",
    "- measurement.latitude: Latitude in degrees\n",
    "- measurement.longitude: Longitude in degrees\n",
    "- site.azimuth: Azimuth angle describing the facing of the instrument on its site\n",
    "- site.campaign: Campaign for this the instrument was at this site (optional)\n",
    "- site.name: Site name (optional)\n",
    "- site.region: Site region (optional)\n",
    "- site.station: Identifier of the station onto which the instrument was installed (optional)\n",
    "\n",
    "### l1b (Advanced users only)\n",
    "\n",
    ">Please note that queries that return raw spectral data will return a substantial volume of data on every row, and as there are typically sixty level 1b spectra on just the measurement channels (10 repeats on six channels), it also returns a lot of rows per measurement. The service is written to happily keep sending results to the requesting client until done, but for most practical purposes we strongly recommend limiting the scope of your request to a narrow window of time and location to keep down the size of the results! This is critically important when you view the results in a web browser rather than saving them directly to file, because it is very easy to make a web browser (which wants to cache the file it is displaying) run completely out of memory! Using a tool like curl to pull something from the api straight into a file on disk should be safe.\n",
    "\n",
    "\n",
    "The request l1b is for advanced users only. Whereas the spectral data that are returned by the GetData request are averaged over 10 spectra and quality screened, the l1b request returns the raw calibrated radiance and irradiance spectra as recorded by the instrument. The return format is fixed, it is not possible to change the columns in the output. However, selecting by date, location and instrument works the same as for the GetData request.\n",
    "> https://wispcloud.waterinsight.nl/api/query?SERVICE=Data&VERSION=1.0&REQUEST=l1b&TIME=2018-07-11T09:00,2018-07-11T19:00&INSTRUMENT=WISPstation001\n",
    "\n",
    "A resulting file will look like this:\n",
    "```\n",
    "# HEADERLINES 17\n",
    "# Request processed at 2018-07-19 09:19:31 UTC\n",
    "# \n",
    "# Requested by cnr from 92.111.24.140\n",
    "# \n",
    "# ---original request---\n",
    "# https://wispcloud.waterinsight.nl/api/query?SERVICE=Data&VERSION=1.0&REQUEST=l1b&TIME=2018-07-11T09:00,2018-07-11T9:30\n",
    "# \n",
    "# ----------------------\n",
    "# Resource: https://wispcloud.waterinsight.nl/api/query\n",
    "# Parameters:\n",
    "#   request ['l1b']\n",
    "#   time ['2018-07-11t09:00,2018-07-11t9:30']\n",
    "#   service ['data']\n",
    "#   version ['1.0']\n",
    "# ----------------------\n",
    "# \n",
    "measurement.id\tmeasurement.date\tmeasurement.latitude\tmeasurement.longitude\tinstrument.name\tchannel.type\tchannel.position\tsite.azimuth\tchannel.orientation\tlevel1.(ir)radiance\n",
    "\tYYYY-MM-DD hh:mm:ss.sssss\tdegree north\tdegree east\t\t\t\tdegree from north\tdegree from instrument azimuth\tW/(m2*nm) if channel.type is Ed else W/(m2*nm*sr) for wavelength [350..900] in 1nm steps\n",
    "84010\t2018-07-11 09:00:05.333859\t56.1981\t-3.3732\tWISPstation003\tEdQC\tB\t337.5\t0.0\t[0.10583016,0.10906596,...,0.08247125]\n",
    "\n",
    "84010\t2018-07-11 09:00:05.333859\t56.1981\t-3.3732\tWISPstation003\tEdQC\tB\t337.5\t0.0\t[0.10479023,0.10830143,...,0.08171204]\n",
    "...\n",
    "```\n",
    "The response of the l1b request is different from the GetData request as each row in the GetData response is one measurement, whereas in the l1b response each row is one separate spectrum. This means we have 60 rows for one single measurement (6 channels: 2 each for Ed, Ld and Lu) with 10 repeat measurements each. In practice, there might even be 70 spectra as there is a reference channel. All the rows of each measurement have the same measurement id, they also include a reference to the channel type (channel.type) and channel postition (channel.position). The combination of channel type and channel position uniquely identifies a channel on the instrument in the following way:\n",
    "- Ed A: Ed at the back 'aft' top of the instrument\n",
    "- Ed F: Ed at the front 'fore' top of the instrument\n",
    "- Ld P: Ld channel at the left 'port' front of the instrument\n",
    "- Ld S: Ld channel at the right 'starboard' front of the instrument\n",
    "- Lu P: Lu channel at the left 'port' front of the instrument\n",
    "- Lu S: Lu channel at the right 'starboard' front of the instrument\n",
    "- (EQC): Reference channel that is not exposed to light, should be ignored\n",
    "\n",
    "Please note that the radiance channels are \"cross-eyed\", i.e. they look across the center line of the instrument. So the right \"starboar\" Lu and Ld channels look at 337.5 degree and the left \"port\" channels at 22.5 from the instrument orientation, usually north. This information is also provided by the site.azimuth and channel.orientation columns. So if the site.azimuth is 180 (facing south) the Lu P channel (channel.orientation 22.5) will be looking at 202.5 degree from north. It is up to the user to construct a dataset from these individual spectra."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
